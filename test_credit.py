import pytest

from credit import credit

@pytest.mark.format
class TestFormat:
	@pytest.mark.parametrize("age", [-1, 2.3])
	def test_age_negative(self, age):
		assert credit(age, "M", "пассивный доход", 1, 1, 1, 1, "ипотека") == (False, 0)

	@pytest.mark.parametrize("sex", ["male", 7])
	def test_sex_negative(self, sex):
		assert credit(1, sex, "пассивный доход", 1, 1, 1, 1, "ипотека") == (False, 0)

	@pytest.mark.parametrize("source", ["male", 7.7])
	def test_source_negative(self, source):
		assert credit(1, "F", source, 1, 1, 1, 1, "ипотека") == (False, 0)

	@pytest.mark.parametrize("income", ["пассивный доход", 4.4])
	def test_income_negative(self, income):
		assert credit(1, "F", "пассивный доход", income, 1, 1, 1, "ипотека") == (False, 0)

	@pytest.mark.parametrize("rating", [-3, 3])
	def test_rating_negative(self, rating):
		assert credit(1, "F", "пассивный доход", 100, rating, 1, 1, "ипотека") == (False, 0)

	@pytest.mark.parametrize("summ", [0, 0.01, 10.1, 11])
	def test_summ_negative(self, summ):
		assert credit(1, "F", "пассивный доход", 100, 1, summ, 1, "ипотека") == (False, 0)

	@pytest.mark.parametrize("term", [0, 7.7, 21])
	def test_term_negative(self, term):
		assert credit(1, "F", "пассивный доход", 100, 1, 3, term, "ипотека") == (False, 0)

	@pytest.mark.parametrize("goal", [0, "done"])
	def test_goal_negative(self, goal):
		assert credit(1, "F", "пассивный доход", 100, 1, 3, 2, goal) == (False, 0)

@pytest.mark.restrictions
class TestRestrictions:
	def test_age_male_restrictions(self):
		assert credit(66, "M", "пассивный доход", 100, 0, 3, 2, "ипотека") == (False, 0)

	def test_age_female_restrictions(self):
		assert credit(63, "F", "пассивный доход", 100, 0, 3, 2, "ипотека") == (False, 0)

	def test_income_restrictions(self):
		assert credit(13, "F", "безработный", 1, -1, 3, 2, "ипотека") == (False, 0)

	def test_rating_restrictions(self):
		assert credit(13, "F", "пассивный доход", 100, -2, 3, 2, "ипотека") == (False, 0)

	def test_source_restrictions(self):
		assert credit(13, "F", "безработный", 100, -1, 3, 2, "ипотека") == (False, 0)


@pytest.mark.sum
class TestSum:
	@pytest.mark.parametrize("source, result", [("собственный бизнес", 10.9), ("пассивный доход", 1.1025), ("наёмный работник", 5.440051499783199)])
	def test_sum_depends_on_source(self, source, result):
		assert credit(1, "F", source, 100, 1, 10, 1, "автокредит") == (True, result)

	@pytest.mark.parametrize("rating, result", [(1, 10.9), (2, 10.85), (-1, 1.1175), (0, 5.477551499783199)])
	def test_sum_depends_on_raiting(self, rating, result):
		assert credit(1, "F", "собственный бизнес", 100, rating, 10, 1, "автокредит") == (True, result)

@pytest.mark.percent
class TestPercent:
	@pytest.mark.parametrize("goal, result", [("развитие бизнеса", 1.1), ("ипотека", 1.085), ("автокредит", 1.105), ("потребительский", 1.12)])
	def test_percent_depends_on_goal(self, goal, result):
		assert credit(1, "F", "пассивный доход", 100, 0, 1, 1, goal) == (True, result), "Некорректная кредитная ставка"
	
	@pytest.mark.parametrize("rating, result", [(0, 1.1), (-1, 1.115), (1, 1.0975), (2, 1.0925)])
	def test_percent_depends_on_rating(self, rating, result):
		assert credit(1, "F", "пассивный доход", 100, rating, 1, 1, "развитие бизнеса") == (True, result), "Некорректная кредитная ставка"

	@pytest.mark.parametrize("source, result", [("пассивный доход", 1.1), ("наёмный работник", 1.0925), ("собственный бизнес", 1.0975)])
	def test_percent_depends_on_source(self, source, result):
		assert credit(1, "F", source, 100, 0, 1, 1, "развитие бизнеса") == (True, result), "Некорректная кредитная ставка"

	@pytest.mark.parametrize("sum, result", [(10, 10.9), (1, 1.1)])
	def test_percent_depends_on_sum(self, sum, result):
		assert credit(1, "F", "собственный бизнес", 100, 1, sum, 1, "автокредит") == (True, result), "Некорректная кредитная ставка"
