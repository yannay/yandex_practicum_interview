from typing import Tuple
from math import log10

def credit(age: int, sex: str, source: str, income: int, rating: int, summ: int, term: int, goal: str) -> Tuple[bool, int]:

	# обработка ошибок
	if age < 0 or type(age) is not int:
		print(f"Некорректный возраст: {age}")
		return (False, 0)
	if sex not in ("M","F"):
		print(f"Некорректный пол: {sex}")
		return (False, 0)
	if source not in ("пассивный доход", "наёмный работник", "собственный бизнес", "безработный"):
		print(f"Некорректный источник дохода: {source}")
		return (False, 0)
	if type(income) is not int:
		print(f"Некорректный доход: {income}")
		return (False, 0)
	if rating not in (-2, -1, 0, 1, 2):
		print(f"Некорректный рейтинг: {rating}")
		return (False, 0)
	if summ < 0.1 or summ > 10:
		print(f"Некорректная сумма: {summ}")
		return (False, 0)
	if term < 1 or term > 20 or type(term) is not int:
		print(f"Некорректный срок погашения: {term}")
		return (False, 0)
	if goal not in ("ипотека", "развитие бизнеса", "автокредит", "потребительский"):
		print(f"Некорректная цель: {goal}")
		return (False, 0)

	# проверяем пенсионный возраст
	if sex == "M" and age > 65:
		print("Не проходит по возрасту")
		return (False, 0)
	if sex == "F" and age > 62:
		print("Не проходит по возрасту")
		return (False, 0)

	# проверяем доход
	if summ/term > income/3:
		print("Не проходит по доходу")
		return (False, 0)

	# проверяем рейтинг
	if rating == -2:
		print("Не проходит по рейтингу")
		return (False, 0)

	# убираем безработных
	if source == "безработный":
		print("Не проходит по источнику дохода")
		return (False, 0)
	
	# расчет одобренной суммы
	summ_result = 10
	if source == "пассивный доход" or rating == -1:
		summ_result = 1
	elif source == "наёмный работник" or rating == 0:
		summ_result = 5
	elif source == "собственный бизнес" or rating == 1 or rating == 2:
		summ_result = 10

	if summ_result > summ:
		summ_result = summ
	
	# расчет ставки
	percent = 10
	if goal == "ипотека":
		percent = percent - 2
	elif goal == "развитие бизнеса":
		percent = percent - 0.5
	elif goal == "потребительский":
		percent = percent + 1.5

	if rating == -1:
		percent = percent + 1.5
	elif rating == 1:
		percent = percent - 0.25
	elif rating == 2:
		percent = percent - 0.75

	percent = percent - log10(summ_result)

	if source == "пассивный доход":
		percent = percent + 0.5
	elif source == "наёмный работник":
		percent = percent - 0.25
	elif source == "собственный бизнес":
		percent = percent + 0.25

	# расчет платежа
	payment = summ_result * (1 + term * percent/100)/ term

	if payment > income/2:
		print("Не проходит по доходу с учетом процентов")
		return (False, 0)

	print(f"Выдан с годовым платежом: {payment}")
	return (True, payment)
	

	

	

	